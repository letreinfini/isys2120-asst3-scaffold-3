#!/usr/bin/env python3
"""
DeviceManagement Database module.
Contains all interactions between the webapp and the queries to the database.
"""

import configparser
import datetime

import setup_vendor_path  # noqa
import pg8000

################################################################################
#   Welcome to the database file, where all the query magic happens.
#   My biggest tip is look at the *week 9 lab*.
#   Important information:
#       - If you're getting issues and getting locked out of your database.
#           You may have reached the maximum number of connections.
#           Why? (You're not closing things!) Be careful!
#       - Check things *carefully*.
#       - There may be better ways to do things, this is just for example
#           purposes
#       - ORDERING MATTERS
#           - Unfortunately to make it easier for everyone, we have to ask that
#               your columns are in order. WATCH YOUR SELECTS!! :)
#   Good luck!
#       And remember to have some fun :D
################################################################################


#####################################################
#   Database Connect
#   (No need to touch
#       (unless the exception is potatoing))
#####################################################

def database_connect():
	"""
	Connects to the database using the connection string.
	If 'None' was returned it means there was an issue connecting to
	the database. It would be wise to handle this ;)
	"""
	# Read the config file
	config = configparser.ConfigParser()
	config.read('config.ini')
	if 'database' not in config['DATABASE']:
		config['DATABASE']['database'] = config['DATABASE']['user']

	# Create a connection to the database
	connection = None
	try:
		# Parses the config file and connects using the connect string
		connection = pg8000.connect(database=config['DATABASE']['database'],
									user=config['DATABASE']['user'],
									password=config['DATABASE']['password'],
									host=config['DATABASE']['host'])
	except pg8000.OperationalError as operation_error:
		print("""Error, you haven't updated your config.ini or you have a bad
		connection, please try again. (Update your files first, then check
		internet connection)
		""")
		print(operation_error)
		return None

	# return the connection to use
	return connection


#####################################################
#   Query (a + a[i])
#   Login
#####################################################

def check_login(employee_id, password):
	"""
	Check that the users information exists in the database.
		- True => return the user data
		- False => return None
	"""

	# Note: this example system is not well-designed for security.
	# There are several serious problems. One is that the database
	# stores passwords directly; a better design would "salt" each password
	# and then hash the result, and store only the hash.
	# This is ok for a toy assignment, but do not use this code as a model when you are
	# writing a real system for a client or yourself.

	# TODO
	# Check if the user details are correct!
	# Return the relevant information (watch the order!)

	# TODO Dummy data - change rows to be useful!
	# NOTE: Make sure you take care of ORDER!!!
	#------------------------------------------------
	if not employee_id.isdigit():
		return None
	conn = database_connect()
	cursor = conn.cursor()
	try:
		cursor.execute('''
			SELECT password
			FROM Employee
			WHERE empid = %s;
		''', (employee_id,))
		fetch = cursor.fetchone()
		if not fetch:
			cursor.close()
			conn.close()
			return None
		if fetch[0] != password:
			cursor.close()
			conn.close()
			return None
		cursor.execute('''
			SELECT empid, name, homeAddress, dateOfBirth
			FROM Employee
			WHERE empid = %s;
		''', (employee_id,))
		fetch = cursor.fetchone()
		user = {
			'empid': fetch[0],
			'name': fetch[1],
			'homeAddress': fetch[2],
			'dateOfBirth': fetch[3],
		}
		cursor.close()
		conn.close()
		return user
	except:
		print('Error')
		return None


#####################################################
#   Query (f[i])
#   Is Manager?
#####################################################

def is_manager(employee_id):
	"""
	Get the department the employee is a manager of, if any.
	Returns None if the employee doesn't manage a department.
	"""

	# TODO Dummy Data - Change to be useful!
	#------------------------------------------------
	conn = database_connect()
	cursor = conn.cursor()
	try:
		cursor.execute('''
			SELECT *
			FROM Department
			WHERE manager = %s;
		''', (employee_id,))
		fetch = cursor.fetchone()
		if not fetch:
			cursor.close()
			conn.close()
			return None
		else:
			cursor.close()
			conn.close()
			return fetch[0];
	except:
		print('Error')
		return None


#####################################################
#   Query (a[ii])
#   Get My Used Devices
#####################################################

def get_devices_used_by(employee_id):
	"""
	Get a list of all the devices used by the employee.
	"""

	# TODO Dummy Data - Change to be useful!
	# Return a list of devices issued to the user!
	# Each "Row" contains [ deviceID, manufacturer, modelNumber]
	# If no devices = empty list []
	#------------------------------------------------
	conn = database_connect()
	cursor = conn.cursor()
	try:
		cursor.execute('''
			SELECT deviceID, manufacturer, modelNumber
			FROM Employee JOIN DeviceUsedBy USING(empID)
				JOIN Device USING(deviceID)
			WHERE empid = %s;
		''', (employee_id,))
		fetch = cursor.fetchall()
		cursor.close()
		conn.close()
		return fetch
	except:
		print('Error')
		return None


#####################################################
#   Query (a[iii])
#   Get departments employee works in
#####################################################

def employee_works_in(employee_id):
	"""
	Return the departments that the employee works in.
	"""

	# TODO Dummy Data - Change to be useful!
	# Return a list of departments
	#------------------------------------------------
	conn = database_connect()
	cursor = conn.cursor()
	try:
		cursor.execute('''
			SELECT department
			FROM EmployeeDepartments
			WHERE empID = %s;
		''', (employee_id,))
		fetch = cursor.fetchall()
		if not fetch:
			cursor.close()
			conn.close()
			return fetch
		dept = []
		for item in fetch:
			dept.append(item[0])
		cursor.close()
		conn.close()
		return dept
	except:
		print('Error')
		return None


#####################################################
#   Query (c)
#   Get My Issued Devices
#####################################################

def get_issued_devices_for_user(employee_id):
	"""
	Get all devices issued to the user.
		- Return a list of all devices to the user.
	"""

	# TODO Dummy Data - Change to be useful!
	# Return a list of devices issued to the user!
	# Each "Row" contains [ deviceID, purchaseDate, manufacturer, modelNumber ]
	# If no devices = empty list []

	#------------------------------------------------
	conn = database_connect()
	cursor = conn.cursor()
	try:
		cursor.execute('''
			SELECT deviceID, purchaseDate, manufacturer, modelNumber
			FROM Employee JOIN Device ON(empid = issuedTo)
			WHERE empid = %s;
		''', (employee_id,))
		fetch = cursor.fetchall()
		cursor.close()
		conn.close()
		return fetch
	except:
		print('Error')
		return None


#####################################################
#   Query (b)
#   Get All Models
#####################################################

def get_all_models():
	"""
	Get all models available.
	"""

	# TODO Dummy Data - Change to be useful!
	# Return the list of models with information from the model table.
	# Each "Row" contains: [manufacturer, description, modelnumber, weight]
	# If No Models = EMPTY LIST []
	#------------------------------------------------
	conn = database_connect()
	cursor = conn.cursor()
	try:
		cursor.execute('''
			SELECT manufacturer, description, modelnumber, weight
			FROM Model;
		''')
		fetch = cursor.fetchall()
		cursor.close()
		conn.close()
		return fetch
	except:
		print('Error')
		return None


#####################################################
#   Query (d[ii])
#   Get Device Repairs
#####################################################

def get_device_repairs(device_id):
	"""
	Get all repairs made to a device.
	"""

	# TODO Dummy Data - Change to be useful!
	# Return the repairs done to a certain device
	# Each "Row" contains:
	#      - repairid
	#      - faultreport
	#      - startdate
	#      - enddate
	#      - cost
	# If no repairs = empty list
	#------------------------------------------------
	conn = database_connect()
	cursor = conn.cursor()
	try:
		cursor.execute('''
			SELECT repairID, faultReport, startDate, endDate, cost
			FROM Repair
			WHERE doneTo = %s;
		''', (device_id,))
		fetch = cursor.fetchall()
		cursor.close()
		conn.close()
		return fetch
	except:
		print('Error')
		return None


#####################################################
#   Query (d[i])
#   Get Device Info
#####################################################

def get_device_information(device_id):
	"""
	Get related device information in detail.
	"""

	# TODO Dummy Data - Change to be useful!
	# Return all the relevant device information for the device
	#------------------------------------------------
	conn = database_connect()
	cursor = conn.cursor()
	try:
		cursor.execute('''
			SELECT *
			FROM Device
			WHERE deviceID = %s;
		''', (device_id,))
		fetch = cursor.fetchone()
		device = {
			'device_id': fetch[0],
			'serial_number': fetch[1],
			'purchase_date': fetch[2],
			'purchase_cost': fetch[3],
			'manufacturer': fetch[4],
			'model_number': fetch[5],
			'issued_to': fetch[6],
		}
		cursor.close()
		conn.close()
		return device
	except:
		print('Error')
		return None


#####################################################
#   Query (d[iii/iv])
#   Get Model Info by Device
#####################################################

def get_device_model(device_id):
	"""
	Get model information about a device.
	"""

	# TODO Dummy Data - Change to be useful!

	#------------------------------------------------
	conn = database_connect()
	cursor = conn.cursor()
	try:
		cursor.execute('''
			SELECT Model.manufacturer, modelNumber, description, weight
			FROM Device JOIN Model USING(modelNumber)
			WHERE deviceID = %s;
		''', (device_id,))
		fetch = cursor.fetchone()
		model = {
			'manufacturer': fetch[0],
			'model_number': fetch[1],
			'description': fetch[2],
			'weight': fetch[3],
		}
		cursor.close()
		conn.close()
		return model
	except:
		print('Error')
		return None


#####################################################
#   Query (e)
#   Get Repair Details
#####################################################

def get_repair_details(repair_id):
	"""
	Get information about a repair in detail, including service information.
	"""

	# TODO Dummy data - Change to be useful!
	#------------------------------------------------
	conn = database_connect()
	cursor = conn.cursor()
	try:
		cursor.execute('''
			SELECT repairID, faultReport, startDate, endDate, cost, abn, serviceName, email, doneTo
			FROM Repair JOIN Service ON(doneBy = abn)
			WHERE repairID = %s;
		''', (repair_id,))
		fetch = cursor.fetchone()
		repair = {
			'repair_id': fetch[0],
			'fault_report': fetch[1],
			'start_date': fetch[2],
			'end_date': fetch[3],
			'cost': fetch[4],
			'done_by': {
				'abn': fetch[5],
				'service_name': fetch[6],
				'email': fetch[7],
			},
			'done_to': fetch[8],
		}
		cursor.close()
		conn.close()
		return repair
	except:
		print('Error')
		return None


#####################################################
#   Query (f[ii])
#   Get Models assigned to Department
#####################################################

def get_department_models(department_name):
	"""
	Return all models assigned to a department.
	"""

	# TODO Dummy Data - Change to be useful!
	# Return the models allocated to the department.
	# Each "row" has: [ manufacturer, modelnumber, maxnumber ]
	#------------------------------------------------
	conn = database_connect()
	cursor = conn.cursor()
	try:
		cursor.execute('''
			SELECT manufacturer, modelNumber, maxNumber
			FROM ModelAllocations
			WHERE department = %s;
		''', (department_name,))
		fetch = cursor.fetchall()
		cursor.close()
		conn.close()
		return fetch
	except:
		print('Error')
		return None


#####################################################
#   Query (f[iii])
#   Get Number of Devices of Model owned
#   by Employee in Department
#####################################################

def get_employee_department_model_device(department_name, manufacturer, model_number):
	"""
	Get the number of devices owned per employee in a department
	matching the model.

	E.g. Model = iPhone, Manufacturer = Apple, Department = "Accounting"
		- [ 1337, Misty, 20 ]
		- [ 351, Pikachu, 10 ]
	"""

	# TODO Dummy Data - Change to be useful!
	# Return the number of devices owned by each employee matching department,
	# manufacturer and model.
	# Each "row" has: [ empid, name, number of devices issued that match ]
	#------------------------------------------------
	conn = database_connect()
	cursor = conn.cursor()
	try:
		cursor.execute('''
			SELECT issuedTo, name, COUNT(*)
			FROM Employee JOIN EmployeeDepartments USING(empID)
				JOIN Device ON(empid = issuedTo)
			WHERE department = %s AND manufacturer = %s AND modelNumber = %s
			GROUP BY issuedTo, name;
		''', (department_name, manufacturer, model_number))
		fetch = cursor.fetchall()
		cursor.close()
		conn.close()
		return fetch
	except:
		print('Error')
		return None


#####################################################
#   Query (f[iv])
#   Get a list of devices for a certain model and
#       have a boolean showing if the employee has
#       it issued.
#####################################################

def get_model_device_assigned(model_number, manufacturer, employee_id):
	"""
	Get all devices matching the model and manufacturer and show True/False
	if the employee has the device assigned.

	E.g. Model = Pixel 2, Manufacturer = Google, employee_id = 1337
		- [123656, False]
		- [123132, True]
		- [51413, True]
		- [8765, False]
	"""

	# TODO Dummy Data - Change to be useful!
	# Return each device of this model and whether the employee has it
	# issued.
	# Each "row" has: [ device_id, True if issued, else False.]
	#------------------------------------------------
	conn = database_connect()
	cursor = conn.cursor()
	try:
		cursor.execute('''
			SELECT deviceID, Device.issuedTo = %s
			FROM Device
			WHERE modelNumber = %s AND manufacturer = %s;
		''', (employee_id, model_number, manufacturer))
		fetch = cursor.fetchall()
		for tuple in fetch:
			if tuple[1] is None:
				tuple[1] = False
		cursor.close()
		conn.close()
		return fetch
	except:
		print('Error')
		return None


#####################################################
#   Query (f[iv])
#   Get a list of devices for this model and
#       manufacturer that have not been assigned.
#####################################################

def get_unassigned_devices_for_model(model_number, manufacturer):
	"""
	Get all unassigned devices for the model.
	"""

	# TODO Dummy Data - Change to be useful!
	# Return each device of this model that has not been issued
	# Each "row" has: [ device_id ]
	#------------------------------------------------
	conn = database_connect()
	cursor = conn.cursor()
	try:
		cursor.execute('''
			SELECT deviceID
			FROM Device
			WHERE modelNumber = %s AND manufacturer = %s
				AND issuedTo IS NULL;
		''', (model_number, manufacturer))
		fetch = cursor.fetchall()
		device = []
		for item in fetch:
			device.append(item)
		cursor.close()
		conn.close()
		return fetch
	except:
		print('Error')
		return None


#####################################################
#   Query (f[iv])
#   Get Employees in Department
#####################################################

def get_employees_in_department(department_name):
	"""
	Return the name of all employees in the department.
	"""

	# TODO Dummy Data - Change to be useful!
	# Return the employees in the department.
	# Each "row" has: [ empid, name ]
	#------------------------------------------------
	conn = database_connect()
	cursor = conn.cursor()
	try:
		cursor.execute('''
			SELECT empid, name
			FROM Employee JOIN EmployeeDepartments USING(empID)
			WHERE department = %s;
		''', (department_name,))
		fetch = cursor.fetchall()
		cursor.close()
		conn.close()
		return fetch
	except:
		print('Error')
		return None


#####################################################
#   Query (f[iv])
#   Get Device Employee Assignment
#####################################################

def get_device_employee_department(manufacturer, modelNumber, department_name):
	"""
	Return the list of devices and who owns them for a given model
	and department.
	"""

	# TODO Dummy Data - Change to be useful!
	# Return the devices matching the manufacturer and model number in the
	# department with the employees assigned.
	# Each row has: [ deviceid, serialnumber, empid, employee name ]
	#------------------------------------------------
	conn = database_connect()
	cursor = conn.cursor()
	try:
		cursor.execute('''
			SELECT deviceID, serialNumber, empid, name
			FROM Device JOIN Employee ON(issuedTo = empid)
				JOIN EmployeeDepartments USING(empid)
			WHERE manufacturer = %s AND modelNumber = %s
				AND department = %s;
		''', (manufacturer, modelNumber, department_name))
		fetch = cursor.fetchall()
		cursor.close()
		conn.close()
		return fetch
	except:
		print('Error')
		return None


#####################################################
#   Query (f[v])
#   Issue Device
#####################################################

def issue_device_to_employee(employee_id, device_id):
	"""
	Issue the device to the chosen employee.
	"""

	# TODO issue the device from the employee
	# Return (True, None) if all good
	# Else return (False, ErrorMsg)
	# Error messages:
	#      - Device already issued?
	#      - Employee not in department?

	# return (False, "Device already issued")
	#------------------------------------------------
	conn = database_connect()
	cursor = conn.cursor()
	cursor.execute('''
		SELECT issuedTo IS NULL
		FROM Device
		WHERE deviceID = %s;
	''', (device_id,))
	fetch = cursor.fetchone()
	if fetch == False:
		cursor.close()
		conn.close()
		return (False, 'Device already issued')
	cursor.execute('''
		SELECT *
		FROM Employee JOIN EmployeeDepartments USING(empid)
		WHERE empid = %s;
	''', (employee_id,))
	fetch = cursor.fetchone()
	if not fetch:
		cursor.close()
		conn.close()
		return (False, 'Employee not in department')
	cursor.execute('BEGIN TRANSACTION;')
	cursor.execute('''
		UPDATE Device
		SET issuedTo = %s
		WHERE deviceID = %s;
	''', (employee_id, device_id))
	cursor.execute('COMMIT;')
	cursor.close()
	conn.close()
	return (True, None)


#####################################################
#   Query (f[vi])
#   Revoke Device Issued to User
#####################################################

def revoke_device_from_employee(employee_id, device_id):
	"""
	Revoke the device from the employee.
	"""

	# TODO revoke the device from the employee.
	# Return (True, None) if all good
	# Else return (False, ErrorMsg)
	# Error messages:
	#      - Device already revoked?
	#      - employee not assigned to device?

	# return (False, "Device already unassigned")
	#------------------------------------------------
	conn = database_connect()
	cursor = conn.cursor()
	cursor.execute('''
		SELECT issuedTo IS NULL
		FROM Device
		WHERE deviceID = %s;
	''', (device_id,))
	fetch = cursor.fetchone()
	if fetch == True:
		cursor.close()
		conn.close()
		return (False, 'Device already revoked')
	cursor.execute('''
		SELECT issuedTo = %s
		FROM Device
		WHERE deviceID = %s
	''', (employee_id, device_id))
	if fetch == False:
		cursor.close()
		conn.close()
		return (False, 'Employee not assigned to device')
	cursor.execute('BEGIN TRANSACTION;')
	cursor.execute('''
		UPDATE Device
		SET issuedTo = NULL
		WHERE deviceID = %s;
	''', (device_id,))
	cursor.execute('COMMIT;')
	cursor.close()
	conn.close()
	return (True, None)

########################################################
########################################################
########################################################
########################################################
########################################################
########################################################
########################################################
########################################################
########################################################
########################################################
def add_new_repair_report(device_id, report, startdate, enddate, cost, svcabn, svcname, svcemail):
	conn = database_connect()
	cursor = conn.cursor()
	cursor.execute('BEGIN TRANSACTION;')
	cursor.execute('''
		SELECT *
		FROM Service
		WHERE abn = %s;
	''', (svcabn,))
	fetch = cursor.fetchone()
	if not fetch:
		cursor.execute('''
			INSERT INTO Service (abn, servicename, email, owed)
			VALUES (%s, %s, %s, '$0.00');
		''', (svcabn, svcname, svcemail))
	elif (fetch[1] != svcname or fetch[2] != svcemail):
		cursor.close()
		conn.close()
		return 'Service information incorrect'
	cursor.execute('''
		SELECT *
		FROM Device
		WHERE deviceID = %s;
	''', (device_id,))
	fetch = cursor.fetchone()
	if not fetch:
		cursor.close()
		conn.close()
		return 'Device does not exist'
	cursor.execute('''
		SELECT COUNT(*)
		FROM Repair
	''')
	repair_id = cursor.fetchone()[0] + 1
	cursor.execute('''
		INSERT INTO Repair (repairID, faultReport, startDate, endDate, cost, doneBy, doneTo)
		VALUES (%s, %s, %s, %s, %s, %s, %s);
	''', (repair_id, report, startdate, enddate, cost, svcabn, device_id))
	cursor.execute('''
		SELECT owed::money::numeric::float8
		FROM Service
		WHERE abn = %s;
	''', (svcabn,))
	owed = cursor.fetchone()[0] + float(cost)
	cursor.execute('''
		UPDATE Service
		SET owed = %s::float8::numeric::money
		WHERE abn = %s;
	''', (owed, svcabn))
	cursor.execute('COMMIT;')
	cursor.close()
	conn.close()
	return True
	
def mycolleagues(employee_id):
	conn = database_connect()
	cursor = conn.cursor()
	retval = []
	cursor.execute('''
		SELECT department
		FROM EmployeeDepartments
		WHERE empid = %s;
	''', (employee_id,))
	department = cursor.fetchall()
	if department:
		for dept in department:
			cursor.execute('''
				SELECT empid, name, homeAddress, dateOfBirth
				FROM Employee JOIN EmployeeDepartments USING(empID)
				WHERE department = %s;
			''', (dept[0],))
			employees = cursor.fetchall()
			if employees:
				for employee in employees:
					retval.append(employee)
	cursor.close()
	conn.close()
	return retval

def empinfo(employee_id):
	conn = database_connect()
	cursor = conn.cursor()
	cursor.execute('''
		SELECT empid, name, homeAddress, dateOfBirth
		FROM Employee
		WHERE empid = %s;
	''', (employee_id,))
	emp = cursor.fetchone()
	cursor.close()
	conn.close()
	return emp
	
def get_phone_number(employee_id):
	conn = database_connect()
	cursor = conn.cursor()
	cursor.execute('''
		SELECT phoneNumber
		FROM EmployeePhoneNumbers
		WHERE empid = %s;
	''', (employee_id,))
	number = cursor.fetchall()
	cursor.close()
	conn.close()
	return number

def employ(name, addr, dob, nos, pwd, dept):
	conn = database_connect()
	cursor = conn.cursor()
	empid = 1
	while True:
		cursor.execute('''
			SELECT *
			FROM Employee
			WHERE empid = %s;
		''', (empid,))
		fetch = cursor.fetchone()
		if fetch:
			empid = empid + 1
		else:
			break
	cursor.execute('BEGIN TRANSACTION;')
	cursor.execute('''
		INSERT INTO Employee (empid, name, homeAddress, dateOfBirth, password)
		VALUES (%s, %s, %s, %s, %s);
	''', (empid, name, addr, dob, pwd))
	cursor.execute('COMMIT;')
	nol = nos.splitlines()
	for no in nol:
		cursor.execute('BEGIN TRANSACTION;')
		cursor.execute('''
			INSERT INTO EmployeePhoneNumbers (empID, phoneNumber)
			VALUES (%s, %s);
		''', (empid, no))
		cursor.execute('COMMIT;')
	cursor.execute('BEGIN TRANSACTION;')
	cursor.execute('''
		INSERT INTO EmployeeDepartments (empID, department, fraction)
		VALUES (%s, %s, 100);
	''', (empid, dept))
	cursor.execute('COMMIT;')
	cursor.close()
	conn.close()
	return empid

########################################################
########################################################
########################################################
########################################################
########################################################
########################################################
########################################################
########################################################
########################################################
########################################################